# gSheets

This repo contains the dataservice and library code for the Google Sheets used
at Tutela. Scripts are written in [Apps Script][1] as `.gs` files, which is used
by all Google Suite products. Read _docs/intro.md_ for an introduction.

Currently, most scripts are written and deployed as APIs to serve data from
spreadsheets, but others are setup to post to Slack or format data. There is a
folder for each deployment and a _library.gs_ file. The library contains common
code between deployments.

Use the deployment folder as templates.

[1] https://www.google.com/script/start/

See [intro](docs/intro.md) for getting started.
