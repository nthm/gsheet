# Setting up a new sheet

First, open a sheet. Then open "Tools" -> "Script Editor" to launch the web IDE.
Name your Apps Script project. Use "/" in the name to have clasp create
directories when the project is pulled onto local systems.

Now, most projects import a library which provides many documented helper
functions for common data aggregation. The library, often called "Util", can be
found in the repo at as _library.gs_. Keep in mind all the functions can be
implemented directly in the one script file - the library is just abstraction.

If you'd like to import it, there are two options.

1. Either open "View" -> "Show manifest file" and paste the contents of
   `appsscript.json` from the repo. Note that it also declares new permissions
   (scopes) required by the library.

2. Or, open "Resources" -> "Libraries" and paste the library project key. Select
   the latest version:

![](./images/libraries.png)

Look at other code from the _deployments/_ folder to get started.

Here's code that defines two hardcoded/static API endpoints, returning the first
column of the sheets "Whitelist" and "Blacklist". It also tracks all edits to
the sheet, and batch posts them to Slack as a form of changelog.

```js
Util.useSpreadsheet(SpreadsheetApp.getActiveSpreadsheet());
// or Util.useSpreadsheet("1sPVUgHUOCcO_yg-RdkE3YAZrkzTZ5...");
// or Util.useSpreadsheet("https://docs.google.com/spreadsheets/d/1sPVU...");

// deploy this!
function doGet(e) {
  var tokens = ['cdfabe', '192731'];
  var publicApi = {
    'whitelist': function () { return Util.columnToArray("Whitelist!A:A") },
    'blacklist': function () { return Util.columnToArray("Blacklist!A:A") }
  }
  return Util.serveJSON(e, tokens, publicApi);
}

// install this!
function installedOnEdit(e) {
  var batchWindow = 1 * 60 * 1000; // 1 min
  return Util.queueSlackNotification('Util', batchWindow, e);
}
```

Next, go to "Publish" -> "Deploy as web app...":

![](./images/deploy.png)

This lets you call your sheet with that URL. If you're using `serveJSON` like
the example code above, you'll need to provide a `?token=XXX?operation=YYY` to
your GET requests. The templates folder shows how to install a dropdown and
modal to display the active APIs for a sheet.

```js
function openModal() {
  Util.openAPIReferenceModal(url, tokens, Object.keys(api))
}

function onOpen() {
  spreadsheet.addMenu('APIs', [
    { name: 'Show API reference', functionName: 'openModal' }
  ])
}
```

Finally, for Slack notifications to track edits to the sheet, install
`installedOnEdit()` as an onEdit trigger under "Edit" -> "All your triggers":

![](./images/triggers/installableTriggers.png)

The algorithm for posting to Slack is:

- On first edit, open a time window of ${batchWindow} time
- All edits that happen in that time window are collected (but not yet posted)
- If an edit is made in the last 10 seconds of a window, add ${batchWindow} time
- When the window expires because people have stopped editing, post all edits to
  Slack.
