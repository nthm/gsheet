## Gotchas

If a script opens or edits the values in a spreadsheet, that sheet's respective
onOpen and onEdit triggers are _not_ fired.

Permissions only matter for the topmost script; the one attached to the sheet.
If a library needs a permission, it must be declared upstream.

When you change permissions of your code, you **must** redeploy, even if the
script is in _dev mode_ (which should hot reload/deploy). Otherwise logs will
just say permission denied.

When choosing a function to call, don't pick onEdit or onOpen - it might seem
like a good fit but it breaks things:

![](./images/triggers/onEditSimpleOnly.png)

If you believe your new OAuth scopes (permissions) are not being picked up, and
you're still being denied permission, try manually adding the scope to the
manifest file `appsscript.json`. A list of scopes can be found here:

https://developers.google.com/apps-script/execution/rest/v1/scripts/run#authorization

I'll paste for convenience:

```
https://apps-apis.google.com/a/feeds
https://apps-apis.google.com/a/feeds/alias/
https://apps-apis.google.com/a/feeds/groups/
https://mail.google.com/
https://sites.google.com/feeds
https://www.google.com/calendar/feeds
https://www.google.com/m8/feeds
https://www.googleapis.com/auth/admin.directory.group
https://www.googleapis.com/auth/admin.directory.user
https://www.googleapis.com/auth/documents
https://www.googleapis.com/auth/documents.currentonly
https://www.googleapis.com/auth/drive
https://www.googleapis.com/auth/dynamiccreatives
https://www.googleapis.com/auth/forms
https://www.googleapis.com/auth/forms.currentonly
https://www.googleapis.com/auth/groups
https://www.googleapis.com/auth/script.cpanel
https://www.googleapis.com/auth/script.external_request
https://www.googleapis.com/auth/script.scriptapp
https://www.googleapis.com/auth/script.send_mail
https://www.googleapis.com/auth/script.storage
https://www.googleapis.com/auth/script.webapp.deploy
https://www.googleapis.com/auth/spreadsheets
https://www.googleapis.com/auth/spreadsheets.currentonly
https://www.googleapis.com/auth/sqlservice
https://www.googleapis.com/auth/userinfo.email
```

Also see https://developers.google.com/apps-script/guides/services/authorization

Check the _images/_ directory for other hiccups as well.
