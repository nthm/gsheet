We now have master and deploy deployments managed through Jenkins via Git
branches. If you merge a PR to 'master' it deploys to the staging URLs in
`deploy.js`, and merging to 'deploy' works as well.

The other branch is 'work'. Where nothing happens.

Note that this only works for redeployments! The **owner** of the spreadsheet
but manually deploy the very first version, and then use `clasp pull` to init
the repo, and add the URL to `deployments.json`.

This is nice for permissions.

If you need to deploy on the web UI, follow the steps outlined in intro.md

For clasp via CLI: **ALWAYS** redeploy using:

```
> clasp deployments
3 Deployments.
- AKfycbznyPnWj_TahnRBuLEF @HEAD
- AKfycbztPLP5TogD0_nCo2tKzo3c4kjY @32 - web app meta-version
- AKfycbyauf17pPe__H4jWQH4b4-82-h1UY0caigVKphM2A @33

16:48:41 ghm@lux ~/p/t/g/d/WhiteBlacklist (1)
> clasp redeploy AKfycbztPLP5TogD0_nCo2tKzo3c4kjY 32 'work'
Updated deployment.

16:48:47 ghm@lux ~/p/t/g/d/WhiteBlacklist (0)
> clasp deployments
3 Deployments.
- AKfycbznyPnWj_TahnRBuLEF @HEAD
- AKfycbyauf17pPe__H4jWQH4b4-82-h1UY0caigVKphM2A @33
- AKfycbztPLP5TogD0_nCo2tKzo3c4kjY @32 - work
```

See https://github.com/google/clasp/issues/63

**Don't** just `clasp deploy`. You will create a new URL. Which no one will use.
It cannot be deleted (as of August 9 2018 see below). Everything's setup to
point to existing URLs.

You cannot undeploy a URL.

```sh
> clasp undeploy 33
Unable to delete read-only deployment.
```

See https://github.com/google/clasp/issues/224 - they're working on it. This was
OK because I reused the accidental deployments as staging URLs.

Deployment URLs/IDs always start with `AKfycb` but will be different lengths.
