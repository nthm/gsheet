# Background

This will go over how to setup your local environment for development and
provides an introduction to the architecture and terms used in Apps Script.

## Apps script

An actively developed extension of the last versioned release of Javscript. The
team has backported _some_ modern ES6+ features as well. There isn't a complete
language reference, but it's safest to write standard ES5 and look at example
code. There are extra classes thrown in as replacements or extensions, for
example, you use `Logger.log()` not `console.log()`.

Extensive APIs are provided to work with each Google Suite application, although
we only use Google Sheets for now. It's a very powerful language, but
documentation can be patchy or confusing, and bugs in the underlying system are
common.

Look at all these APIs!
```
> clasp apis list
abusiveexperiencereport   - abusiveexperiencereport:v1
acceleratedmobilepageurl  - acceleratedmobilepageurl:v1
adexchangebuyer           - adexchangebuyer:v1.4
adexchangebuyer2          - adexchangebuyer2:v2beta1
adexchangeseller          - adexchangeseller:v2.0
adexperiencereport        - adexperiencereport:v1
admin                     - admin:reports_v1
adsense                   - adsense:v1.4
adsensehost               - adsensehost:v4.1
analytics                 - analytics:v3
analyticsreporting        - analyticsreporting:v4
androiddeviceprovisioning - androiddeviceprovisioning:v1
androidenterprise         - androidenterprise:v1
androidmanagement         - androidmanagement:v1
androidpublisher          - androidpublisher:v3
appengine                 - appengine:v1
appsactivity              - appsactivity:v1
appstate                  - appstate:v1
bigquery                  - bigquery:v2
bigquerydatatransfer      - bigquerydatatransfer:v1
blogger                   - blogger:v3
books                     - books:v1
calendar                  - calendar:v3
chat                      - chat:v1
civicinfo                 - civicinfo:v2
classroom                 - classroom:v1
cloudbilling              - cloudbilling:v1
cloudbuild                - cloudbuild:v1
clouddebugger             - clouddebugger:v2
clouderrorreporting       - clouderrorreporting:v1beta1
cloudfunctions            - cloudfunctions:v1
cloudiot                  - cloudiot:v1
cloudkms                  - cloudkms:v1
cloudprofiler             - cloudprofiler:v2
cloudresourcemanager      - cloudresourcemanager:v2
cloudshell                - cloudshell:v1
cloudtasks                - cloudtasks:v2beta2
cloudtrace                - cloudtrace:v2
composer                  - composer:v1beta1
compute                   - compute:v1
container                 - container:v1
content                   - content:v2
customsearch              - customsearch:v1
dataflow                  - dataflow:v1b3
dataproc                  - dataproc:v1
datastore                 - datastore:v1
deploymentmanager         - deploymentmanager:v2
dfareporting              - dfareporting:v3.1
dialogflow                - dialogflow:v2
digitalassetlinks         - digitalassetlinks:v1
discovery                 - discovery:v1
dlp                       - dlp:v2
dns                       - dns:v1
doubleclickbidmanager     - doubleclickbidmanager:v1
doubleclicksearch         - doubleclicksearch:v2
drive                     - drive:v3
firebasedynamiclinks      - firebasedynamiclinks:v1
firebaserules             - firebaserules:v1
firestore                 - firestore:v1
fitness                   - fitness:v1
fusiontables              - fusiontables:v2
games                     - games:v1
gamesConfiguration        - gamesConfiguration:v1configuration
gamesManagement           - gamesManagement:v1management
genomics                  - genomics:v1
gmail                     - gmail:v1
groupsmigration           - groupsmigration:v1
groupssettings            - groupssettings:v1
iam                       - iam:v1
iamcredentials            - iamcredentials:v1
iap                       - iap:v1beta1
identitytoolkit           - identitytoolkit:v3
indexing                  - indexing:v3
jobs                      - jobs:v2
kgsearch                  - kgsearch:v1
language                  - language:v1
licensing                 - licensing:v1
logging                   - logging:v2
manufacturers             - manufacturers:v1
mirror                    - mirror:v1
ml                        - ml:v1
monitoring                - monitoring:v3
oauth2                    - oauth2:v2
oslogin                   - oslogin:v1
pagespeedonline           - pagespeedonline:v4
partners                  - partners:v2
people                    - people:v1
playcustomapp             - playcustomapp:v1
plus                      - plus:v1
plusDomains               - plusDomains:v1
poly                      - poly:v1
proximitybeacon           - proximitybeacon:v1beta1
pubsub                    - pubsub:v1
redis                     - redis:v1beta1
replicapool               - replicapool:v1beta1
replicapoolupdater        - replicapoolupdater:v1beta1
reseller                  - reseller:v1
runtimeconfig             - runtimeconfig:v1
safebrowsing              - safebrowsing:v4
script                    - script:v1
searchconsole             - searchconsole:v1
servicebroker             - servicebroker:v1
serviceconsumermanagement - serviceconsumermanagement:v1
servicecontrol            - servicecontrol:v1
servicemanagement         - servicemanagement:v1
serviceusage              - serviceusage:v1
serviceuser               - serviceuser:v1
sheets                    - sheets:v4
siteVerification          - siteVerification:v1
slides                    - slides:v1
sourcerepo                - sourcerepo:v1
spanner                   - spanner:v1
spectrum                  - spectrum:v1explorer
speech                    - speech:v1
sqladmin                  - sqladmin:v1beta4
storage                   - storage:v1
storagetransfer           - storagetransfer:v1
streetviewpublish         - streetviewpublish:v1
surveys                   - surveys:v2
tagmanager                - tagmanager:v2
tasks                     - tasks:v1
testing                   - testing:v1
texttospeech              - texttospeech:v1beta1
toolresults               - toolresults:v1beta3
tpu                       - tpu:v1
translate                 - translate:v2
urlshortener              - urlshortener:v1
vault                     - vault:v1
videointelligence         - videointelligence:v1
vision                    - vision:v1
webfonts                  - webfonts:v1
webmasters                - webmasters:v3
websecurityscanner        - websecurityscanner:v1alpha
youtube                   - youtube:v3
youtubeAnalytics          - youtubeAnalytics:v2
youtubereporting          - youtubereporting:v1
```

The service provides a web IDE, which is useful for autocomplete and debugging.
However, it's recommended to develop scripts locally where they can be tracked
in Git and later synced to scripts.google.com. Syncing is done using _clasp_,
the Apps Scripts CLI, which is used to pull, push, and deploy scripts.

Google has a codelab to introduce clasp, and goes over how to install it to your
system. It's a TypeScript application, so you'll need a Node:

https://codelabs.developers.google.com/codelabs/clasp/

---

## Terms and concepts

Below are notes on terms and concepts learned developing the first scripts in
April 2018. Keep in mind that the language is very actively developed so it's
possible some things have changed - the most common reason seems to be for
security concerns, requiring the team to restrict or deprecate APIs.

### Types:

There are two types of scripts.

- *Standalone* : The script shows up in Google Drive but does not get to access
any services like spreadsheets or documents - the special classes like
`SpreadsheetApp` exist but are `{}` so it's not possible to interactive with
other services. You can write a script to call a standalone script.

- *Bundled* : Means it's attached to a document or spreadsheet and only exists
as long as it does. Note that a bundled script is *not* restricted to accessing
only its document - any URL or ID can be passed as long as it's authorized to
open it (which can be messy).

### Deployments:

- As a *web app* : Functions named `doGet()` and `doPost()` will be used to
  respond to GET and POST requests from web clients. They must return text or
  HTML. The HTML can be a full template/page that is populated with data and can
  even include frontend JS to interact with the gScript again; making it easy to
  deploy a web UI. We use this to deploy APIs.

- As an *API executable* : For servers (Java, NodeJS, Python, ...) to remotely
  call public functions in the script (not just `do{Get,Post}()`) for data. The
  API will return strings, numbers, objects, anything but *special* Google
  service classes like `SpreadsheetApp`. Google has posted starter code in 10+
  languages for interacting with these API scripts. The response is slightly
  restricted to have a `{ done: bool, error?: '', response?: {} }` layout. We
  have yet to use this, but it would allow for a more direct binding to the
  script from a programming langauge.

When you deploy you get a link. With each deployment method there's an option
to always point to the latest code release (instead of a specific version).
Permissions can be messy, and will cause the deployment to redirect to Google
login pages if it's not deployed under _your_ account.

### Libraries:

You can share scripts as libraries. It's as easy as passing the script ID and
selecting what version you'd like to use. This is great for code reuse, and we
use one library to back several sheets each with their own API.

Note that using a library is a lot like copy and pasting it directly in the
including script - permissions required by a library, and triggers created in a
library must be provided by the including script, not the library.

![](./images/libraries.png)

### Triggers

When you read about or see _onEdit_ or _onOpen_ functions, these are run when
the document or sheet is edited or opened, respectfully. These aren't the only
triggers.

![](./images/triggers/triggers.png)

https://developers.google.com/apps-script/guides/triggers/#available_types_of_triggers

In that link you'll see that there's an edit and open event for **both** simple
and installable triggers. Simple triggers are triggers that are generated for
you when you create a function called onEdit or onOpen etc. **They're limited
and must not run code that requires user permission**. Meanwhile, installable
triggers are explicitly installed by you in the script editor window:

![](./images/triggers/installableTriggers.png)

### State

Scripts are inherently stateless. If you want to maintain state, such as growing
a stack on each request, you'll need to use `PropertiesService` which is a
string-only key-value store either per document or per connecting user.

There's good documentation, however, they don't clearly mention that it is
string only:

![](./images/stringsOnlyPropertiesService.png)

Read _docs/gotchas.md_ for other notes and do-nots.
