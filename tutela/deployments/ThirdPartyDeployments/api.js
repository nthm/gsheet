var spreadsheet = SpreadsheetApp.getActiveSpreadsheet()
Util.useSpreadsheet(spreadsheet)

// you don't set this, it's given to you when you deploy as a web app
// it's only here for the modal to display working links. nothing more
var url = 'https://script.google.com/a/<DOMAIN>/macros/s/<HASH>/exec'
var tokens = ['xxx', 'xxx']
var sheetName = 'Known Partners'

// each api function is able to receive a params object too
var api = {
  // the `3` is to skip the headers and jump right ot the data
  'api-key': function() {
    return Util.columnToArray(sheetName + '!A3:A')
  },
  'parameter': function() {
    return Util.columnToArray(sheetName + '!B3:B')
  },
  'partner-name': function() {
    return Util.columnToArray(sheetName + '!C3:C')
  },
  'all': function() {
    return Util.rowsAsJSON(sheetName + '!A3:D', [
      'api-key', 'parameter', 'partner-name', 'partner-report'])
  },
  // this doesn't make sense without the sheet, but shows that the resolver can
  // be complex if you need it to be
  'packages-for-api-key': function(params) {
    var key = params['api-key']
    if (!key) {
      return 'error: no "&api-key=" given'
    }
    // object indexes 0-3 are Name, Deployment Key, API Key, Package Name
    return Util.rowsAsJSON('Sheet!C3:F')
      .filter(function(obj) { return obj[2] === key })
      .map(function(obj) {
        return {
          PackageName: obj[3],
          Name: obj[0],
        }
      })
  },
}

// eslint-disable-next-line no-unused-vars
function doGet(event) {
  return Util.serveJSON(event, tokens, api)
}

// eslint-disable-next-line no-unused-vars
function openModal() {
  Util.openAPIReferenceModal(url, tokens, Object.keys(api))
}

// eslint-disable-next-line no-unused-vars
function onOpen() {
  spreadsheet.addMenu('APIs', [
    {
      name: 'Show API reference',
      functionName: 'openModal',
    },
  ])
}
