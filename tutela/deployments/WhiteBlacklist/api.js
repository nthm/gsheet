var spreadsheet = SpreadsheetApp.getActiveSpreadsheet()
Util.useSpreadsheet(spreadsheet)

// you don't set this, it's given to you when you deploy as a web app
// it's only here for the modal to display working links. nothing more
var url = 'https://script.google.com/a/<DOMAIN>/macros/s/<HASH>/exec'
var tokens = ['xxx', 'xxx']
var api = {}

// take each sheet name, lowercase it, and replace spaces with underscores
// this means you don't need to define basic relations like:

// 'whitelist': function() {
//   return Util.rangeToValues('Whitelist!A:A')
// }

spreadsheet.getSheets()
  .map(function(sheet) { return sheet.getName() })
  .forEach(function(name) {
    // default to returning a list of values of the first column of the sheet
    api[name.replace(/\W+/g, '_').toLowerCase()] = function() {
      return Util.columnToArray(name + '!A:A')
    }
  })

// use this to override that behaviour or define unusual relations
var overides = {
  'some_tokens': function() {
    return Util.rangeToValues('Sheet Name!A:C')
  },
  // backwards compatability with old clients
  'some-tokens': function() {
    Util.sendSlackNotification('Received a request to the deperecated API endpoint "some-tokens". Please update your data service to remove the hyphen')
    return Util.rangeToValues('Sheet Name!A:C')
  },
}

// there is no Object.assign in this version of JS, loop manually
Object.keys(overides).forEach(function(key) {
  api[key] = overides[key]
})

// eslint-disable-next-line no-unused-vars
function doGet(event) {
  return Util.serveJSON(event, tokens, api)
}

// eslint-disable-next-line no-unused-vars
function openModal() {
  Util.openAPIReferenceModal(url, tokens, Object.keys(api))
}

// eslint-disable-next-line no-unused-vars
function onOpen() {
  spreadsheet.addMenu('APIs', [
    {
      name: 'Show API reference',
      functionName: 'openModal',
    },
  ])
}

// install this as an onEdit trigger via: `Edit` > `All triggers`
// eslint-disable-next-line no-unused-vars
function installedOnEdit(event) {
  var batchWindow = 1 * 60 * 1000 // 1 min
  // need to pass the name the libary is imported as so it can setup a trigger here
  return Util.trackChanges('Util', batchWindow, event)
}
