const { execSync } = require('child_process')
const path = require('path')

const urls = {
  staging: {
    WhiteBlacklist: 'AKfycbyauf...FZ2KphM2A',
    ThirdPartyDeployments: 'AKfycbwfN...8CUlNoGUE_',
  },
  production: {
    WhiteBlacklist: 'AKfycbzt...2tKzo3c4kjY',
    ThirdPartyDeployments: 'AKfycbyM...GfT083CeMY1g',
  },
}

// if you need actual argument parsing use minimist
const argv = process.argv.slice(2)

const branches = Object.keys(urls)
let branch

if (argv.length !== 1 || !branches.includes(branch = argv.pop())) {
  console.log('Usage: `node deploy <branch>` or `npm run deploy -- <branch>`')
  console.log('Where valid branches are:', branches)
  process.exit(1)
}

Object.entries(urls[branch]).forEach(([directory, url]) => {
  let stdout
  const sh = command => {
    stdout = execSync(command, {
      cwd: path.resolve(__dirname, directory),
    }).toString()
    console.log(stdout)
  }

  sh(`clasp deployments | grep ${url}`)

  // avoiding regex to match between '@' and ' ' from `AKfy...kjY @32 - desc`
  const version = Number(stdout.split('@').pop().split(' ')[0])
  console.log(`${directory} deployed version is ${version}`)

  sh('clasp version')
  sh(`clasp redeploy ${url} ${version + 1} "auto deploy to ${branch}"`)
})
