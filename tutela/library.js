// Spreadsheet library of helper functions

// Merged with tokens provided by the client and used in serveJSON. Setting
// values to this array will be like creating master keys
var allTokens = []
// The reference to the Spreadsheet object
var spreadsheet
// Not stored in the state to make changing it easier; otherwise it would need a
// setProperty(). Don't change this value here - use setSlackChannel()
var slackChannel = '#channel-name'

// Used as constants for PropertiesService
var STATE = {
  queue: 'SlackPayloadQueue',
  triggerTime: 'SlackTriggerMillisecondTime',
  triggerId: 'SlackTriggerId',
}

/**
 * Simplifies openBy{Id,Url}
 *
 * @param {string|Spreadsheet} value Spreadsheet to attach to
 */

// eslint-disable-next-line no-unused-vars
function useSpreadsheet(value) {
  if (typeof value === 'object') {
    spreadsheet = value
    return
  }
  if (typeof value === 'string') {
    // support both docs.google.com/sheets and sheets.google.com
    var sheetIsUrl = value.indexOf('google.com') > -1
    spreadsheet = sheetIsUrl
      ? SpreadsheetApp.openByUrl(value)
      : SpreadsheetApp.openById(value)
    return
  }
  throw 'Told to use an invalid spreadsheet'
}

function _error(message) {
  return { error: message }
}

function _server(request, api) {
  // see https://developers.google.com/apps-script/guides/web
  var parameters = request.parameter
  var token = parameters.token
  if (!token) {
    return _error('Must provide an auth token')
  }
  var hasValidToken = false
  for (var i = 0; i < allTokens.length; i++) {
    if (token === allTokens[i]) {
      hasValidToken = true
      break
    }
  }
  if (!hasValidToken) {
    return _error('Invalid token')
  }
  var operation = parameters.fn || parameters.operation
  var options = Object.keys(api).toString()
  if (!operation) {
    return _error('Specify "operation" parameter. Valid options: ' + options)
  }
  if (!api[operation]) {
    return _error('"' + operation + '" is not part of the API.  Valid options: ' + options)
  }
  return { result: api[operation](parameters) }
}

/**
 * JSON server to deploy as a web app. Forward requests from doGet() to this
 *
 * Use 'Publish > Deploy as web app...' and configure it to run as yourself:
 * "Me youremail@domain.com" and allow access to: "Anyone, even anonymous"
 *
 * Set some tokens to keep it secure
 *
 * JSON schema: if there's an error, like a bad token, returns { error: ... },
 * otherwise { data: ... } with the result of the API's function
 *
 * @param {Event} request The incoming request passed on from doGet()
 * @param {Array} tokens List of valid tokens to authenticate requests
 * @param {Object} api Users will call the service with ?operation=name. Keys of
 * this object are the available names and their values will be **called** (as
 * functions!) with their results returned in the JSON. See schema above
 * @return {Response} The JSON as a response to pass back to doGet
 */

// eslint-disable-next-line no-unused-vars
function serveJSON(request, tokens, api) {
  for (var i = 0; i < tokens.length; i++) {
    allTokens.push(tokens[i])
  }
  var result = _server(request, api)
  return ContentService.createTextOutput(JSON.stringify(result))
    .setMimeType(ContentService.MimeType.JSON)
}

/**
 * Simplifies Range.getValues()
 *
 * @param {string|Range} range To attach to
 * @return {Array} Range.getValues() result
 */
function rangeToValues(range) {
  // eslint-disable-next-line valid-typeof
  if (typeof range !== 'Range') {
    range = spreadsheet.getRange(range)
  }
  return range.getValues()
}

/**
 * Search for text in a range. Case insensitive.
 *
 * @param {string|Range} the range of the sheet to search in
 * @param {string} text to look for
 * @return {boolean} true if the text exists
 */

// eslint-disable-next-line no-unused-vars
function existsInRange(range, text) {
  var data = rangeToValues(range)
  var found = false
  text = text.toLowerCase()
  rows: for (var i = 0; i < data.length; i++) {
    // can't use data[i].indexOf() because it needs to be case insensitive
    for (var j = 0; j < data[i].length; j++) {
      if (data[i][j].toLowerCase() === text) {
        found = true
        Logger.log('found, breaking loop')
        break rows
      }
    }
  }
  Logger.log('result is ' + found)
  return found
}

/**
 * Convert a range to an array JSON'd rows with columns as keys
 * Either provide headers or the relative column index is used
 *
 * Example where only two of the three columns are defined:
 * rowsAsJSON("A1:C2", ['name', 'age'])
 * > [{ name: '', age: '', 2: '' }, { name: '', age: '', 2: '' }]
 *
 * @param {string|Range} a range object or string of one
 * @param {Array} used as column names; optional
 * @return {Array} the array of JSON'd rows
 */

// eslint-disable-next-line no-unused-vars
function rowsAsJSON(range, headers) {
  if (!headers) {
    headers = []
  }
  var data = rangeToValues(range)
  var result = []
  for (var i = 0; i < data.length; i++) {
    var row = {}
    for (var j = 0; j < data[i].length; j++) {
      var value = data[i][j]
      if (value === 'FALSE') {
        value = false
      }
      if (value === 'TRUE') {
        value = true
      }
      row[headers[j] || j] = value
    }
    result.push(row)
  }
  return result
}

/**
 * Convert a range to an array of its first column (vertical entries)
 *
 * Examples:
 * A1:B5    returns entries in rows 1 to 5 of column A
 * C10:C50  returns entries in rows 10 to 50 of column C
 * D        returns all entries (all rows) of column D
 * 1        is invalid
 *
 * @param {string|Range} a range object or string of one
 * @return {Array} entries from the column's rows
 */

// eslint-disable-next-line no-unused-vars
function columnToArray(range) {
  var data = rangeToValues(range)
  // could also set range to exactly one column and flatten .getValues()
  var result = []
  for (var i = 0; i < data.length; i++) {
    var value = data[i][0]
    if (value !== '') {
      result.push(value)
    }
  }
  return result
}

/**
 * Convert a range to an array of its first row (horizontal entries)
 *
 * Examples:
 * A1:B5    returns entries in columns A to B of row 1
 * C10:C50  returns entries in column C of row 10
 * D        is invalid
 * 1        returns all entries (all columns) of row 1
 *
 * @param {Range} range Any valid range notation
 * @return {Array} The array of entries from the row's columns
 */

// eslint-disable-next-line no-unused-vars
function rowToArray(range) {
  var data = rangeToValues(range)
  var result = []
  for (var i = 0; i < data.length; i++) {
    var value = data[0]
    if (value !== '') {
      result.push(value)
    }
  }
  return result
}

/**
 * Changes the Slack channel to post edits to. Defaults to #channel-name
 *
 * @param {String} channel The channel to post to. With or without '#' in front
 */

// eslint-disable-next-line no-unused-vars
function setSlackChannel(channel) {
  var withHashChannel = channel.charAt(0) === '#' ? channel : '#' + channel
  slackChannel = withHashChannel
  // var state = PropertiesService.getDocumentProperties();
  // state.setProperty(STATE.channel, withHashChannel);
}

/**
 * Track changes made to a spreadsheet.
 *
 * The cells will be marked-up with modification notices.
 * Edits will also be queued for a batch post to Slack.
 *
 * Hook this as an installable onEdit trigger (not just `onEdit() {}`!)
 *
 * @param {String} libraryName Triggers are created in the client script land so
 * the function to post to Slack (here in library land) needs to be prefixed
 * with whatever name the client used... i.e 'Util' -> 'Util.postQueueToSlack'
 * @param {Integer} batchWindow The time in milliseconds from the last edit to
 * post to Slack
 * @param {Event} event The event from an onEdit trigger
 */

// eslint-disable-next-line no-unused-vars
function trackChanges(libraryName, batchWindow, event) {
  Logger.log('Received edit event: ' + JSON.stringify(event))
  // sometimes they come in as null?
  if (!event) {
    return
  }

  Logger.log(event.source.getName())

  var spreadsheet = event.source
  var sheet = event.source.getActiveSheet()
  var range = event.source.getActiveRange().getA1Notation()

  // modifying a range, rather than a cell, produces an event with no '.value'
  // or '.oldValue' fields. check for that here...
  var isMultiEdit = range.indexOf(':') > -1
  if (!event.value && !event.oldValue && !isMultiEdit) {
    Logger.log('Empty change. Skipping.')
    return
  }

  var now = new Date()
  var modificationMessage = Utilities.formatString('Last modified: %s\nBy: %s', now, event.user)
  event.range.setNote(modificationMessage)

  var details = ''
  if (isMultiEdit) {
    details = '_Details are not provided for range edits_'
  } else if (event.oldValue && !event.value) {
    details = ':x: Removed: "' + event.oldValue + '"'
  } else if (event.oldValue) {
    details = ':construction: From "' + event.oldValue + '" to "' + event.value + '"'
  } else {
    details = ':new: New value: "' + event.value + '"'
  }
  var sheetLink = Utilities.formatString('<%s#gid=%s|%s>', spreadsheet.getUrl(), sheet.getSheetId(), sheet.getName())
  var slackMessage = Utilities.formatString('%s on %s to %s at %s\n%s', event.user, sheetLink, range, now.toLocaleTimeString(), details)

  // These scripts don't maintain state. Have to work some magic to reduce spam.
  var state = PropertiesService.getDocumentProperties()
  Logger.log(state.getProperty(STATE.triggerId))

  var queue = JSON.parse(state.getProperty(STATE.queue) || '[]')
  if (queue.length === 0) {
    var validSlackLink = Utilities.formatString('Updates to the sheet: <%s|%s>', spreadsheet.getUrl(), spreadsheet.getName())
    queue.push(validSlackLink)
  }
  queue.push(slackMessage)
  state.setProperty(STATE.queue, JSON.stringify(queue))
  Logger.log('added payload to the slack queue: ' + JSON.stringify(queue))

  // Postpone notification if edits happen shortly before sending a notifiction
  var currentTriggerTime = state.getProperty(STATE.triggerTime)
  if (currentTriggerTime) {
    if (parseInt(currentTriggerTime) > Date.now() - 10 * 1000) {
      Logger.log('Not postponing trigger')
      return
    }

    Logger.log('Event occured too close to the trigger. Postponing it')
    _clearTimeTriggers()
    state.deleteProperty(STATE.triggerId)
    state.deleteProperty(STATE.triggerTime)
  }

  // make a new trigger. note triggers cannot be passed arguments
  var trigger = ScriptApp.newTrigger(libraryName + '.sendQueuedSlackNotifications')
    .timeBased()
    .after(batchWindow)
    .create()

  state.setProperty(STATE.triggerId, trigger.getUniqueId())
  state.setProperty(STATE.triggerTime, (Date.now() + batchWindow).toString())
  Logger.log('Set new states')
}

// it's very buggy to remove the trigger by its ID. remove all time based ones
function _clearTimeTriggers() {
  var triggers = ScriptApp.getProjectTriggers()
  for (var i = 0; i < triggers.length; i++) {
    var current = triggers[i]
    // don't remove the onEdit trigger - that'll break the script
    if (current.getEventType() === ScriptApp.EventType.CLOCK) {
      ScriptApp.deleteTrigger(current)
    }
  }
}

/**
 * Send the stored edit queue to Slack.
 *
 * Works by joining the queue into one string and sending it to postToSlack()
 */

// eslint-disable-next-line no-unused-vars
function sendQueuedSlackNotifications() {
  var state = PropertiesService.getDocumentProperties()
  var property = state.getProperty(STATE.queue)
  if (!property) {
    Logger.log('No queue to send to slack.')
    return
  }

  var queue = JSON.parse(property)
  Logger.log(queue)

  _clearTimeTriggers()
  state.deleteProperty(STATE.queue)
  state.deleteProperty(STATE.triggerId)
  state.deleteProperty(STATE.triggerTime)

  queue.push('---')
  sendSlackNotification(queue.join('\n'))
}

/**
 * Post one message to Slack
 *
 * @param {String} slackMessage The text to appear. Supports markdown and
 * friends. See Slack's documentation
 */
function sendSlackNotification(slackMessage) {
  spreadsheet.toast('Notifying Slack')
  var url = 'https://hooks.slack.com/services/<WEBHOOK_URL>'
  var options = {
    method: 'post',
    contentType: 'application/json',
    payload: JSON.stringify({
      text: slackMessage,
      channel: slackChannel,
      icon_emoji: ':hibiscus:',
    }),
  }
  UrlFetchApp.fetch(url, options)
  Logger.log('OK sent')
}

/**
 * Renders a modal explaining how to use the API. To call this, you'll likely
 * want to have a dropdown in the sheets UI. It's not easy to do that from a
 * library (it requires telling the library what it's own namespace is similar
 * to `postQueueToSlack`) so do it your application like this:
 *
 * function openModal() {
 *   Util.openAPIReferenceModal('myUrl', ['myToken', ...], ['getUsers', ...])
 * }
 * function onOpen() {
 *   yourSpreadsheet.addMenu('APIs', [
 *     { name: 'Show API reference', functionName: 'openModal' }
 *   ])
 * }
 *
 * @param {String} url The URL of the deployed web application
 * @param {Array} tokens List of tokens set outside of the library
 * @param {Array} api List of API operations (likely use `Object.keys(api)`)
 */

// eslint-disable-next-line no-unused-vars
function openAPIReferenceModal(url, tokens, operations) {
  // url will follow `https://script.google.com/...6zRKhqhU/exec`
  var urlMerged = url + '?token=TOKEN&operation=OPERATION'
  var urlWithToken = urlMerged.replace('TOKEN', tokens[0])
  var html =
  '<html><body>'
  + '<style>'
  + 'body { font-family: sans-serif; }'
  + 'code { padding: 3px 5px; margin: 0 2px; background: lightgrey; line-height: 25px; }'
  + '</style>'
  + '<p>Replace <code>TOKEN</code> and <code>OPERATION</code> in this URl to call the API for JSON:</p>'
  + '<p style="font-family: monospace; word-break: break-all;">' + urlMerged + '</p>'
  + '<p>Available tokens:' + tokens.map(function(token) { return '<code>' + token + '</code>' }).join(', ') + '</p>'
  + '<p>Available operations:' + operations.map(function(op) { return '<code>' + op + '</code>' }).join(', ') + '</p>'
  + '<p>Links to each operation using the first token: (open in new tab)</p>'
  + '<ul>'
  + operations.map(function(op) {
    return (
      '<li><code>'
      + '<a href="' + urlWithToken.replace('OPERATION', op) + '" target="blank" onclick="google.script.host.close()">' + op + '</a>'
      + '</code></li>'
    )
  }).join('')
  + '</ul></body></html>'
  var ui = HtmlService.createHtmlOutput(html)
  SpreadsheetApp.getUi().showModalDialog(ui, 'API Reference')
}
