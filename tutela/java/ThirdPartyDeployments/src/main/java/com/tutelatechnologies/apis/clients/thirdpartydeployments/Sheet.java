package com.tutelatechnologies.apis.clients.thirdpartydeployments;

import com.google.gson.JsonElement;
import com.tutelatechnologies.apis.clients.BaseClient;

public class Sheet extends BaseClient<String> {
    public Sheet() {
        super(
          "https://script.google.com/macros/s/YourWonderfulSheetIDHere/exec?",
          "YourToken"
        );
    }
    @Override
    public String parseItem(JsonElement rawItem) {
        return rawItem.getAsString();
    }
}
