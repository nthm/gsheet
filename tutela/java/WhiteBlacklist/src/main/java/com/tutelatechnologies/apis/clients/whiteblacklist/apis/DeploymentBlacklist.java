package com.tutelatechnologies.apis.clients.whiteblacklist.apis;

import com.tutelatechnologies.apis.clients.whiteblacklist.SimpleSheet;

public class DeploymentBlacklist extends SimpleSheet {

    /**
     * @param url
     * @param token
     */
    public DeploymentBlacklist(final String url, final String token) {
        super("deployment_blacklist", url, token);
    }
}
