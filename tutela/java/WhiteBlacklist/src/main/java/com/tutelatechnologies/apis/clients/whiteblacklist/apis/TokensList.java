package com.tutelatechnologies.apis.clients.whiteblacklist.apis;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.tutelatechnologies.apis.clients.whiteblacklist.Sheet;
import com.tutelatechnologies.apis.clients.whiteblacklist.apis.TokensList.TokenEntry;

public class TokensList extends Sheet<TokenEntry> {

    /**
     * @param url
     * @param token
     */
    public TokensList(final String url, final String token) {
        super("tokens", url, token);
    }

    @Override
    public TokenEntry parseItem(JsonElement rawItem) {
        JsonArray array = rawItem.getAsJsonArray();
        return new TokenEntry(
                array.get(0).getAsString(),
                array.get(1).getAsString(),
                Scope.valueOf(array.get(2).getAsString()));
    }

    /*
     * Supporting classes.
     */
    public static enum Scope {
        Write,
        ReadWrite
    }

    public static class TokenEntry {
        public final String token;
        public final String issuedTo;
        public final Scope scope;

        public TokenEntry(final String token, final String issuedTo, final Scope scope) {
            this.token = token;
            this.issuedTo = issuedTo;
            this.scope = scope;
        }
    }
}
