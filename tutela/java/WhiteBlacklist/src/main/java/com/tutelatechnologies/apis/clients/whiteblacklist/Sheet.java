package com.tutelatechnologies.apis.clients.whiteblacklist;

import java.util.List;

import com.tutelatechnologies.apis.clients.BaseClient;
import com.tutelatechnologies.apis.clients.ClientException;

public abstract class Sheet<T> extends BaseClient<T> {
    private final String operation;

    /**
     * @param operation
     * @param url
     * @param token
     */
    protected Sheet(final String operation, final String url, final String token) {
        super(url, token);
        this.operation = operation;
    }

    public final List<T> get() throws ClientException {
        return fetchValues(operation);
    }
}
