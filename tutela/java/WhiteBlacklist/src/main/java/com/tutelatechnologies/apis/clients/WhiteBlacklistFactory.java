package com.tutelatechnologies.apis.clients;

import com.tutelatechnologies.apis.clients.whiteblacklist.apis.DeploymentBlacklist;
import com.tutelatechnologies.apis.clients.whiteblacklist.apis.PrivacyCompliantDeploymentKeyList;
import com.tutelatechnologies.apis.clients.whiteblacklist.apis.SDKWhitelist;
// ... continue for all APIs

/**
 * @author Tony and Grant
 */
public final class WhiteBlacklistFactory {

    private static final String URL = "https://script.google.com/macros/s/YourWonderfulSheetIDHere/exec?";
    private static final String TOKEN = "YourToken";

    private WhiteBlacklistFactory() {
    }

    public static DeploymentBlacklist getDeploymentBlacklist() {
        return new DeploymentBlacklist(URL, TOKEN);
    }

    public static PrivacyCompliantDeploymentKeyList getPrivacyCompliantDeploymentKeyList() {
        return new PrivacyCompliantDeploymentKeyList(URL, TOKEN);
    }

    public static SDKWhitelist getSDKWhitelist() {
        return new SDKWhitelist(URL, TOKEN);
    }

    // ... continue for all APIs
}
