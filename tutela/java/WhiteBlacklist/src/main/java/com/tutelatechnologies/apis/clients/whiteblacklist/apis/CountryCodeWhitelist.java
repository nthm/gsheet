package com.tutelatechnologies.apis.clients.whiteblacklist.apis;

import com.tutelatechnologies.apis.clients.whiteblacklist.SimpleSheet;

public class CountryCodeWhitelist extends SimpleSheet {

    /**
     * @param url
     * @param token
     */
    public CountryCodeWhitelist(final String url, final String token) {
        super("country_code_whitelist", url, token);
    }
}
