package com.tutelatechnologies.apis.clients.whiteblacklist;

import com.google.gson.JsonElement;

public abstract class SimpleSheet extends Sheet<String> {

    /**
     * @param operation
     * @param url
     * @param token
     */
    protected SimpleSheet(final String operation, final String url, final String token) {
        super(operation, url, token);
    }

    @Override
    public final String parseItem(JsonElement rawItem) {
        return rawItem.getAsString();
    }
}
