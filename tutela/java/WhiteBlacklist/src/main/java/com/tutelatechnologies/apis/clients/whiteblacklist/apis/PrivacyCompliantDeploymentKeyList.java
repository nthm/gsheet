package com.tutelatechnologies.apis.clients.whiteblacklist.apis;

import com.tutelatechnologies.apis.clients.whiteblacklist.SimpleSheet;

public class PrivacyCompliantDeploymentKeyList extends SimpleSheet {

    /**
     * @param url
     * @param token
     */
    public PrivacyCompliantDeploymentKeyList(final String url, final String token) {
        super("privacy_deployment_key_whitelist", url, token);
    }
}
