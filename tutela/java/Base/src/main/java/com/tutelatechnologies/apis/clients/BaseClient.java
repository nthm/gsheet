package com.tutelatechnologies.apis.clients;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.slf4j.Logger;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.HttpUrl;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.ResponseBody;
import com.tutelatechnologies.dashboard.DataUsageLogs.Logging;

/**
 * Standard setup for a client.
 *
 * @author Tony and Grant
 * @param <T>
 */
public abstract class BaseClient<T> {
    private static final Logger LOG = Logging.getLogger(BaseClient.class);
    private static final String PARAM_OPERATION = "operation";
    private static final String PARAM_TOKEN = "token";
    private static final String RESULT_KEY = "result";

    private final String sheetURL;
    private final String token;

    /**
     * @param sheetURL
     * @param token
     */
    protected BaseClient(
        final String sheetURL,
        final String token
    ) {
        this.sheetURL = sheetURL;
        this.token = token;
    }

    /**
     * @return All values in the list
     * @throws ClientException if unable to fetch or parse the list
     */
    // TODO(ghm): allow additional query params for a given operation
    public List<T> fetchValues(
        final String operation
        // final HashMap<String, String> addtionalQueryParams
    ) throws ClientException {
        JsonArray response = getResponseAsJsonArray(operation);
        if (response == null || response.size() == 0) {
            return Collections.emptyList();
        }

        removeHeaders(response);
        return StreamSupport
            .stream(response.spliterator(), false)
            .map(this::parseItem)
            .collect(Collectors.toList());
    }

    private void removeHeaders(final JsonArray response) {
        // First row in the response will be the column headers
        response.remove(0);
    }

    /**
     * Parse an item from the list that the server responded with.
     *
     * @param rawItem Item from the list
     * @return Parsed response item
     */
    public abstract T parseItem(JsonElement rawItem);

    private JsonArray getResponseAsJsonArray(
        final String operation
        // final HashMap<String, String> addtionalQueryParams
    ) throws ClientException {
        final String response = fetchResponse(operation);

        return new JsonParser()
            .parse(response)
            .getAsJsonObject()
            .getAsJsonArray(RESULT_KEY);
    }

    private String fetchResponse(
        final String operation
        // final HashMap<String, String> addtionalQueryParams
    ) throws ClientException {
        HttpUrl url = HttpUrl
            .parse(sheetURL)
            .newBuilder()
            .addQueryParameter(PARAM_TOKEN, token)
            .addQueryParameter(PARAM_OPERATION, operation)

        // for (Map.Entry<String, String> pair : addtionalQueryParams.entrySet()) {
        //     url.addQueryParameter(pair.getKey(), pair.getValue());
        // }

            .build();
        Request request = new Request.Builder()
            .url(url)
            .build();

        try {
            final OkHttpClient httpclient = createClient();
            final Call call = httpclient.newCall(request);

            Response response = call.execute();
            LOG.info("Response: {}", response.message());

            try (ResponseBody body = response.body()) {
                final String bodyString = body.string();
                return bodyString;
            }
        } catch (final IOException | IllegalArgumentException e) {
            throw new ClientException("Request failed", e);
        }
    }

    private static OkHttpClient createClient() {
        OkHttpClient toReturn = new OkHttpClient();
        toReturn.setFollowRedirects(true);
        toReturn.setFollowSslRedirects(true);
        return toReturn;
    }
}
