package com.tutelatechnologies.apis.clients;

/**
 * Thrown when a list request fails.
 */
public class ClientException extends Exception {
    /**
     * @param message
     *            Explains why exception was thrown
     */
    public ClientException(String message) {
        super(message);
    }

    /**
     * @param message
     *            Explains why exception was thrown
     * @param cause
     *            Exception that caused the error
     */
    public ClientException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * @param cause
     *            Exception that caused the error
     */
    public ClientException(Throwable cause) {
        super(cause);
    }
}
