Removed the tests and some APIs. Redacted the Sheet tokens and URLs.

```
├── Base
│   ├── src
│   │   └── main
│   │       └── java
│   │           └── com
│   │               └── tutelatechnologies
│   │                   └── apis
│   │                       └── clients
│   │                           ├── BaseClient.java
│   │                           └── ClientException.java
│   └── pom.xml
├── ThirdPartyDeployments
│   ├── src
│   │   └── main
│   │       └── java
│   │           └── com
│   │               └── tutelatechnologies
│   │                   └── apis
│   │                       └── clients
│   │                           ├── thirdpartydeployments
│   │                           │   ├── apis (empty)
│   │                           │   └── Sheet.java
│   │                           └── ThirdPartyDeploymentsFactory.java
│   ├── test
│   │   └── java
│   │       └── com
│   │           └── tutelatechnologies
│   │               └── apis
│   │                   └── clients
│   │                       └── ThirdPartyDeploymentsFactoryTest.java
│   └── pom.xml
├── WhiteBlacklist
│   ├── src
│   │   └── main
│   │       └── java
│   │           └── com
│   │               └── tutelatechnologies
│   │                   └── apis
│   │                       └── clients
│   │                           ├── whiteblacklist
│   │                           │   ├── apis
│   │                           │   │   ├── CountryCodeWhitelist.java
│   │                           │   │   ├── DeploymentBlacklist.java
│   │                           │   │   ├── PrivacyCompliantDeploymentsList.java
│   │                           │   │   └── TokensList.java
│   │                           │   ├── Sheet.java
│   │                           │   └── SimpleSheet.java
│   │                           └── WhiteBlacklistFactory.java
│   ├── test
│   │   └── java
│   │       └── com
│   │           └── tutelatechnologies
│   │               └── apis
│   │                   └── clients
│   │                       └── WhiteBlacklistFactoryTest.java
│   └── pom.xml
├── pom.xml
└── readme.md
```
