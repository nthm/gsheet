# gSheets

Google App Script projects. Provides a web API (that you can `curl`) to read out
sheet data as JSON. It also keeps track of what changes were made by who, when,
and posts the changelogs to a Slack channel.

This was developed for internal use with Tutela around early 2018. Keep in mind
that Apps Script is actively developed, and the API, permission scopes, and
language features are still changing - this may not work in the future.

## Features

- Sets up a web API that can be called to return data from the sheet.
- Basic authentication through hardcoded tokens.
- An in-sheet dropdown menu and modal dialog to show let users know what API
  calls are available.
- Post a bulk changelog to Slack. Uses a cooldown to preventing spamming
  messages to a channel.

See the two projects' readme files for information.
