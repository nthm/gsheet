// ESS lockers


// HELLO.
// There's a lot of loose ends in this file. It's a blueprint of what was to be
// implemented. Also, it was meant as a backend only, replacing MySQL+PHP.


// Notation here can be a little confusing between "Spreadsheet" and "Sheet"
var ss = SpreadsheetApp.getActiveSpreadsheet()
// System can be in either REGISTER or RENEW mode. The term is also saved, via getTerm() i.e "Fall 2018"
var state = PropertiesService.getScriptProperties()

// Get, and renew
function doGet(event) {
  if (event) {
    return ContentService.createTextOutput(JSON.stringify(event))
  }

  // Step 1: Parse parameters. What kind of data is being requested?
  var request = event.request

  // request.body
  // ...
  switch (request.body.operation) {
    case 'listAvailableLockers': {
      return listAvailableLockers()
    }
    case 'renewLocker': {
      return renewLocker(request.body.token)
    }
    case 'turnInLocker': {
      return renewLocker(request.body.token)
    }
  }
}

// Set
function doPost(event) {
  if (event) {
    return ContentService.createTextOutput(JSON.stringify(event))
  }
}

function onOpen() {
  // TODO: Have a modal for this? With explanations
  ss.addMenu('Locker mode', [
    { name: 'Renew', functionName: 'openModal' },
    { name: 'Register', functionName: 'openModal' },
  ])
}

function renewLocker(token) {
  // It's likely someone will click the renew link in their email after the window has closed:
  if (PropertiesService.getDocumentProperties('mode') !== 'RENEW') {
    // Error: Lockers are no longer open for renewal, sorry. Sign up for a new one instead.
  }
  if (!token) {
    // Error: Must provide a token
  }
  // If it's not found then also error and say so
  // Bump expiry of the locker
  // Bump number of consecutive terms they've had the locker
}

function listAvailableLockers() {
  // For loop checking a field. A few lines at most.
}

function isAvailable(lockerNumber) {
  // Ditto. Return true/false.
}

// From this call onward, the term is over and the term sheet is now read-only
function initiateNewTerm() {
  var currentSheet = ss.getSheets()[0]

  var currentSheetName = currentSheet.getName()
  var [term, year] = currentSheetName.split(' ')
  var terms = ['Spring', 'Summer', 'Fall']
  var newTerm = terms[(terms.indexOf(term) + 1) % terms.length]
  var newSheetName = newTerm + ' ' + (Number(year) + 1).toString()

  // Copy current sheet into a new one; set the new current
  var newSheet = ss.insertSheet(newTerm, 0, { template: currentSheet })
  ss.setActiveSheet(newSheet)
  // Lock the old sheet as read-only
  currentSheet.protect()

  // TODO: For each locker, if their locker is set to expire this term, email
  // them their renewal token and set the expiry to 'NOW'

  // Open renewals (closes registration)
  state.setProperty('mode', 'RENEW')
}

function finalizeNewTerm() {
  // TODO: For each locker, if expiry is 'NOW' then email them that they lost
  // their locker, and it will be cut etc. Delete the row.

  state.setProperty('mode', 'REGISTER')
}

// Delete term sheets, which is OK since you can always Ctrl-Z
function clearPreviousTerms(numberToKeep) {
  var terms = ss.getSheets()
  Logger.log('There are ' + terms.length + ' terms. Keeping ' + numberToKeep)
  while (terms.length <= numberToKeep) {
    ss.deleteSheet(terms.pop())
  }
}

// Token _might_ already exist. Super rare, but best to be safe
function generateToken() {
  // TODO: Get the sheet for the current term. Not just the first sheet
  var term = ss.getSheets()[0]
  var currentTokens = []
  var token = null
  do {
    // From: http://stackoverflow.com/questions/8532406/ddg#8532436
    token = Math.random().toString(36).substr(2)
  } while (currentTokens.indexOf(token) !== -1)
  
  return token
}

// TODO: Should this take a spreadsheet row instead? Where is it called?
function sendEmail(to) {
  var replyTo = 'ess@uvic.ca'

  // This depends on the purpose of the email. Maybe use a switch statement to decide between messages
  // Unfortunately, AppsScript doesn't support template strings like ECMAScript does
  var subject = 'You\'ve got mail, ' + replyTo
  var body = 'Hello here\'s your locker details'

  // Docs: https://developers.google.com/apps-script/reference/mail/mail-app#sendemailto-replyto-subject-body
  MailApp.sendEmail(to, replyTo, subject, body)
}

function notifySlack(message) {
  var url = 'https://hooks.slack.com/services/<WEBHOOK_URL>';
  var options = {
    'method': 'post',
    'contentType': 'application/json',
    'payload': JSON.stringify({
      text: message,
      channel: '#lockers',
      icon_emoji: ':hibiscus:'
    })
  }
  UrlFetchApp.fetch(url, options)
}
