# ESS lockers

_Archived. Last updated September 2018._ Someone slid in with a nice update to
the existing MySQL+PHP system by adding a React frontend. If it falls through
I'll pick this up again.

---

Keeps track of what students register lockers. Sends emails to them at the end
of a term allowing them to renew their lease, else be dropped from the sheet.

This is just the backend. There's an existing frontend for the old PHP+MySQL
system, and it has been tweaked to talk to this script instead. Students see no
change, but staff now use the spreadsheet to much more easily manage lockers.

## Features

 - Entirely automated 
 - Keeps history; currently for up to 2 years/6 terms; uses a sheet per term
 - Supports _reserved_ and _broken_ lockers (by setting them to no expiry)
 - Posts signups to Slack
 - Analytics baked in via script.google.com dashboard

## Notes

The web frontend where students request a locker needs to know which ones are
available, what term it is, and a way of submitting data into the system. This
will be done with doGet and doPost functions, respectively.

The next hurdle was deciding how to know what term it is. Considered converting
the real date to determine that. See 5fe4839:

```js
function getTerm() {
  var now = new Date()
  var [year, month, day] = now.toISOString().substr(0, 10).split('-').map(Number)
  
  Logger.log((function() {
    if (now <= new Date('March 20 ' + year)) return 'Spring ' + year
    if (now <= new Date('August 10 ' + year)) return 'Summer ' + year
    if (now <= new Date('December 15 ' + year)) return 'Fall ' + year
  })())
}
```

Not only would it make it very hard to debug, but it's also more complex and
harder to understand what's going on under the hood.

Switched to a simple circular array of terms to loop through. That _forces_ the
leftmost sheet to be the current one - there's no finding the current term based
on the date. If the system ever gets out of sync and needs to be intervened,
just set the name of the first sheet and roll from there.

Likewise, expiry dates should be terms (ie Fall 2018) not specific dates or
timestamps. Then the comparision is simplified to the year and an array index of
either Fall/Spring/Summer.
